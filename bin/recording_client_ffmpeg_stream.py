#/usr/bin/env python3
# -*- coding: utf-8 -*-

# Example:
# Usage:
# Same as _live but add --inputfile URL instead of an audio device
# TODO rework whole script, merge with live

import argparse
import os
import os.path
import re
import signal
import subprocess
import sys
import traceback
import urllib.request, urllib.parse, urllib.error
from datetime import datetime
from threading import Thread

from pythonrecordingclient.session import Session, on_session_ready, on_receive
from pythonrecordingclient.mediatorLib import Auth
from pythonrecordingclient.config import Config

# from recording_client_live import get_title_from_api
print_output = False


def stop_gracefully(signal, frame):
    print('Signal caught, exiting.')
    if ObjectStore.isRunning == 0:
        print("Recordingclient was already supposed to stop but did not. Forcing exit.")
        sys.exit()
    else:
        ObjectStore.isRunning = 0
        # isRunning has no effect if we're not done connecting yet
        if ObjectStore.session is not None and not ObjectStore.session.ready:
            ObjectStore.session.stop()

# Class for storing pointers to objects


class ObjectStore(object):
    RATE = 16000
    inputFile = ""
    CHUNKSIZE = 1024
    stream = None
    volume = 1
    isRunning = 1

    def __init__(self):
        pass

    @staticmethod
    def getStream():
        if ObjectStore.stream == None:
            args = [
                "ffmpeg",
                # be less verbose (but still show stats)
                "-hide_banner", "-loglevel", "warning", "-stats",
            ]
            args += [
                # ignore video tracks
                #"-re",
                #"-rtsp_transport", "tcp",
                "-vn",
                "-i", ObjectStore.inputFile,
                # use the first audio channel
                "-map", "0:a",
                "-ac", "1",
                # adjust volume
                "-filter:a", "volume={volume}".format(
                    volume=ObjectStore.volume),
                # convert to 16kHz signed little endian, one audio channel only
                "-f", "s16le",  "-ar", str(
                    ObjectStore.RATE), "-c:a",  "pcm_s16le",
                # output to sdout
                "-"
            ]
            print(f"Starting ffmpeg process with command: {args}")
            ObjectStore.ffmpegProcess = subprocess.Popen(
                args, stdout=subprocess.PIPE, stdin=subprocess.PIPE)
            ObjectStore.stream = ObjectStore.ffmpegProcess.stdout
            print("Started")

        return ObjectStore.stream

    @staticmethod
    def close():
        if ObjectStore.stream is not None:
            ObjectStore.ffmpegProcess.terminate()
            ObjectStore.ffmpegProcess.wait()


def verify_chunk_size(value):
    try:
        val = int(value)
        assert(val > 0)
    except:
        val = value
        raise argparse.ArgumentTypeError(
            '%s is an invalid positive int value' % value)
    return val


@on_receive
def session_receive(session, el):
    if not print_output or el.find('text').text is None:
        return
    start = el.get('start')
    stop = el.get('stop')
    creator = el.get('creator')
    fingerprint = el.get('fingerprint')
    text = urllib.parse.unquote(el.find('text').text)
    formatted = ' | '.join([creator, str(session.id), fingerprint, start, stop]) + ': ' + text
    print(formatted)


@on_session_ready
def session_ready(session):
    print('READY')
    t1 = Thread(target=send_audio, args=(session, ))
    t1.daemon = True
    t1.start()


def send_audio(session):
    try:
        print('session ready (%d), sending audio..' % (session.id,))
        while ObjectStore.isRunning:
            res = ObjectStore.getStream().read(ObjectStore.CHUNKSIZE)
            success = session.send_audio(res)
            if len(res) == 0 or success is False:
                break
        ObjectStore.close()
        sys.exit()
    except IOError as e:
        print('IOError in stream.read()')
        print(e)
        try:
            ObjectStore.close()
        except:
            pass
        sys.exit()


def main(args):
    global print_output

    if args.hall:
        title = get_title_from_api(args.hall, args.api)
    else:
        title = args.title

    print_output = args.printoutput

    output_fingerprint = args.outfingerprint.split(
        ',') if args.outfingerprint else []
    session = Session(name=title, description=args.desc, password=args.password, input_fingerprint=args.fingerprint,
                      output_fingerprint=output_fingerprint, input_types=['audio'], logging=args.store,
                      interactive=True, zoom_url=args.zoomurl, zoom_quick_cc=args.zoomquickcc,
                      auth = Auth(args.user, args.passw, args.authserver) if args.user is not None else None)

    ObjectStore.inputFile = args.inputfile
    ObjectStore.volume = args.volume
    ObjectStore.CHUNKSIZE = args.chunksize

    print('SESSION START')
    session.start(args.server, args.port)
    session.interface.socket.t.join(99999)
    sys.exit(session.interface.exit_code)


# If we are not running interactively
if __name__ == '__main__':

    signal.signal(signal.SIGINT, stop_gracefully)

    parser = argparse.ArgumentParser(description='RecordingClient that streams live audio from a chosen audio interface',
                                     formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('-S', '--server', help='server address')
    parser.add_argument('-H', '--hall', help='lecture hall id', default='')
    parser.add_argument('-A', '--api', help='URL to event API',
                        default='https://lecture-translator.kit.edu')
    parser.add_argument('-p', '--port', type=int, help='port', default=4443)
    parser.add_argument(
        '-t', '--title', help='title of the session', default='TestSession')
    parser.add_argument(
        '-d', '--desc', help='description of the session', default='')
    parser.add_argument('-w', '--password',
                        help='password of the session', default='secret')
    parser.add_argument('-fi', '--fingerprint',
                        help='fingerprint of the input stream', default='de-DE')
    parser.add_argument('-fo', '--outfingerprint',
                        help='fingerprint of the output stream', default='')
    parser.add_argument('-po', '--printoutput',
                        help='print the output stream', action='store_true')
    parser.add_argument(
        '-s', '--store', help='store session in database', action='store_true')
    parser.add_argument(
        '-r', '--record', help='record session in database (not implemented)', action='store_true')
    parser.add_argument('-c', '--chunksize', type=verify_chunk_size,
                        help='size of chunks, that are sent to the mediator', default=1024)
    parser.add_argument('-l', '--log', help='logging level (not implemented)',
                        default='info', choices=['critical', 'error', 'warn', 'info', 'debug'])
    parser.add_argument(
        '-i', '--inputfile', help='input file that will be read by ffmpeg, can be in any file or live stream that ffmpeg can handle', required=True)
    parser.add_argument(
        '--volume', help='adjust the input file volume', type=float, default=1.0)
    parser.add_argument('-o', '--outputfile',
                        help='dump audio to file (not implemented)', default='')
    parser.add_argument('--zoomurl', help='Zoom video conference url for supplying subtitles', default=None, type=str)
    parser.add_argument('--zoomquickcc', help='Display Zoom subtitles in a fast, but not reliable way', action='store_true')
    parser.add_argument('--proxy', help="url to a http tunnel proxy 'https?://\w+(:\d+)?'", default=None, type=str)
    parser.add_argument('--user', help="Username for connecting with the authentication server. Needs to be used with --pass. Setting enables authentication.", default=None, type=str)
    parser.add_argument('--pass', help="Password for connecting with the authentication server. If not supplied you will be asked to enter it.", default=None, type=str, dest="passw")
    parser.add_argument('--authserver', help="Server used for logging in with --user and --pass", default="https://auth.lecture-translator.com", type=str)
    args = parser.parse_args()
    main(args)
