#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# Dependency: PyAudio (make sure to have macports 2014 version installed, 2011 version won't work)
# Example:
# Usage:
# - First list all available audio devices in order to select the correct one:
# > python recording_client.py -L 1
# Start recording using default parameters and audio device 1:
# > python recording_client.py -S i13srv30.ira.uka.de -d Description -a 1
# Or select audio device via specifying the (partial) name:
# > python recording_client.py -S i13srv30.ira.uka.de -d Description -a microph

import argparse
import logging
import os
import signal
import sys
import urllib.request, urllib.parse, urllib.error
import time
import numpy as np
from datetime import datetime
from threading import Thread
from typing import Union, List, Dict, Optional, BinaryIO

import pyaudio # type: ignore
import requests

from pythonrecordingclient.session import Session, on_session_ready, on_receive
from pythonrecordingclient.mediatorLib import Auth
from pythonrecordingclient.config import Config
# from pythonrecordingclient.watchdog import Watchdog

print_output = False
watchdog = None
logger = logging.getLogger('prc')

def stop_gracefully(signal, frame):
    logger.info('Signal caught, exiting.')
    if ObjectStore.isRunning == 0:
        logger.warning("Recordingclient was already supposed to stop but did not. Forcing exit.")
        sys.exit()
    else:
        ObjectStore.isRunning = 0
        # isRunning has no effect if we're not done connecting yet
        if ObjectStore.session is not None and not ObjectStore.session.ready:
            ObjectStore.session.stop()
        elif ObjectStore.isReadBlocking:
            Thread(target=readBlockWorkaround, name="readBlockWorkaround", daemon=True).start()

def readBlockWorkaround() -> None:
    """
    This is a workaround for the blocking portaudio read function.
    If the audio read function in send_audio keeps blocking, isRunning
    will never be checked and the program hangs.
    So we skip most of the cleanup that would happen in send_audio and
    just signal the session to stop, which results in all remaining non-daemon threads
    being closed and therefore having the daemon-processes including the
    blocking send_audio loop stopped uncleanly too.

    A real solution would be to switch to using the non-blocking API of portaudio,
    but currently there's not enough reason to switch.
    """
    # Wait a second and check again to see if we're really blocked or just had bad luck
    import time
    time.sleep(1)
    if ObjectStore.isReadBlocking:
        logger.warning("Audio read is blocking, using workaround to stop.")
        if ObjectStore.session is not None:
            ObjectStore.session.stop()
        else:
            # Shikataganai
            logger.warning("Workaround failed. Forcing close")
            sys.exit()


# Class for storing pointers to objects
class ObjectStore(object):
    FORMAT = pyaudio.paInt16
    CHANNELS = 1
    CHOSEN_CHANNEL: Optional[int] = None
    RATE = 16000
    INPUT_DEVICE = 2
    CHUNKSIZE = 1024
    pAudio: Optional[pyaudio.PyAudio]   = None
    stream: Optional[pyaudio.Stream]    = None
    isRunning = 1
    isReadBlocking = False # For a workaround, see comment in stop_gracefully

    outfile: Union[None, bool, BinaryIO] = None

    # Session object is usually used as a parameter but we need a global ref for stop_gracefully
    session: Optional[Session] = None

    def __init__(self):
        pass

    @staticmethod
    def setInputDevice(id: str) -> None:
        devices = get_audio_devices()
        try:
            ObjectStore.INPUT_DEVICE = int(id)
            devName = devices[int(id)]
            print('Audio input device:', id, '(', devName, ')')
        except (ValueError, KeyError):
            hits = []
            for key in devices:
                if id.lower() in devices[key].lower():
                    hits.append(key)
            if len(hits)==1:
                ObjectStore.INPUT_DEVICE = hits[0]
                print('Audio input device:', hits[0], '(', devices[hits[0]], ')')
            else:
                if len(hits)==0:
                    print('ERROR: unknown audio device:', id)
                elif len(hits)>1:
                    print('ERROR: ambiguous audio device:', id)
                print_all_devices()
                sys.exit(1)

    @staticmethod
    def setAudioChannelFilter(channel: int) -> None:
        # actually chosing a specific channel is apparently impossible with portaudio,
        # so we record all channels instead and then filter out the wanted channel with numpy
        p = ObjectStore.getPyAudio()
        channelCount = p.get_device_info_by_host_api_device_index(0, ObjectStore.INPUT_DEVICE).get('maxInputChannels')
        print('Recording channel', channel, 'of', channelCount)
        ObjectStore.CHANNELS = channelCount
        ObjectStore.CHOSEN_CHANNEL = channel
    @staticmethod
    def getPyAudio():
        if ObjectStore.pAudio == None:
            ObjectStore.pAudio = pyaudio.PyAudio()

        return ObjectStore.pAudio

    @staticmethod
    def getStream() -> pyaudio.Stream:
        if ObjectStore.stream == None:
            p = ObjectStore.getPyAudio()
            ObjectStore.stream = p.open(
                format              = ObjectStore.FORMAT,
                input_device_index  = ObjectStore.INPUT_DEVICE,
                channels            = ObjectStore.CHANNELS,
                rate                = ObjectStore.RATE,
                input               = True,
                frames_per_buffer   = ObjectStore.CHUNKSIZE)

        return ObjectStore.stream


def verify_chunk_size(value: Union[str, int]) -> int:
    try:
        val: int = int(value)
        assert(val > 0)
    except:
        raise argparse.ArgumentTypeError('%s is an invalid positive int value' % value)
    return val

def configure_logging(level) -> None:
    logger.setLevel(level)
    logger.propagate = False
    ch = logging.StreamHandler(sys.stdout)
    ch.setLevel(level)
    formatter = logging.Formatter(fmt='[{asctime}][{levelname:^8}]: {message}', datefmt='%H:%M:%S', style='{')
    ch.setFormatter(formatter)
    logger.addHandler(ch)


@on_receive
def session_receive(session: Session, el) -> None:
    if not print_output or el.find('text').text is None:
        return
    start = el.get('start')
    stop = el.get('stop')
    creator = el.get('creator')
    fingerprint = el.get('fingerprint')
    text = urllib.parse.unquote(el.find('text').text)
    formatted = ' | '.join([creator, str(session.id), fingerprint, start, stop]) + ': ' + text
    if watchdog:
        watchdog.received_text(fingerprint, formatted)
    print(formatted)


@on_session_ready
def session_ready(session: Session) -> None:
    print('READY')

    if ObjectStore.outfile:
        try:
            ObjectStore.outfile = open('{}-{}-{}.raw'.format(datetime.now().isoformat(timespec='minutes'), session.id, session.title), mode='wb')
        except OSError as e:
            logger.warning("Could not open file {} to write audio. Reason: {}".format(e.filename, e.strerror))

    t1 = Thread(target=send_audio, args=(session, ))
    t1.daemon = True
    t1.start()

def send_audio(session: Session) -> None:
    try:
        print(f'session ready ({session.id}), sending audio..')
        while ObjectStore.isRunning:
            ObjectStore.isReadBlocking = True
            chunk = ObjectStore.getStream().read(ObjectStore.CHUNKSIZE, exception_on_overflow=False)
            ObjectStore.isReadBlocking = False

            if ObjectStore.CHOSEN_CHANNEL is not None:
                # filter out specific channel using numpy
                logging.info("Using numpy to filter out specific channel.")
                data = np.fromstring(chunk, dtype='int16').reshape((-1, ObjectStore.CHANNELS))
                data = data[:, ObjectStore.CHOSEN_CHANNEL - 1]
                if watchdog:
                    watchdog.sent_audio(data)
                chunk = data.tostring()

            if ObjectStore.outfile:
                try:
                    if isinstance(ObjectStore.outfile, BinaryIO):
                        ObjectStore.outfile.write(chunk)
                    else:
                        logger.warning("Outfile is not writable, disabling. This is possibly a bug")
                        ObjectStore.outfile = False
                except OSError as e:
                    logger.warning("Could not write audio to file. Reason: {}".format(e.strerror))

            session.send_audio(chunk)
        logger.info("Exited audio loop. Cleaning up...")
        session.stop()
        logger.debug("Stopped session")
        if ObjectStore.stream is not None:
            ObjectStore.stream.stop_stream()
        logger.debug("Stopped pyaudio stream")
        if ObjectStore.stream is not None:
            ObjectStore.stream.close()
        logger.debug("Closed pyaudio stream")
        if ObjectStore.pAudio is not None:
            ObjectStore.pAudio.terminate()
        logger.debug("Terminated pyaudio")
        if ObjectStore.outfile:
            try:
                if isinstance(ObjectStore.outfile, BinaryIO):
                    ObjectStore.outfile.close()
                    logger.debug("Closed file")
            except Exception as e:
                logger.warning("Could not close file. Error follows")
                logger.warning(e)
        logger.info("Done. Goodbye")
    except IOError as e:
        logger.error('IOError in stream.read(), may be caused by an outdated version portaudio?')
        try:
            if ObjectStore.stream is not None:
                ObjectStore.stream.stop_stream()
                ObjectStore.stream.close()
            if ObjectStore.pAudio is not None:
                ObjectStore.pAudio.terminate()
        except Exception as e:
            logger.error("Unhandled error:")
            logger.error(e)
        sys.exit()


def get_audio_devices() -> Dict[int, str]:
    devices = {}

    p = ObjectStore.getPyAudio()
    info = p.get_host_api_info_by_index(0)
    deviceCount = info.get('deviceCount')

    for i in range(0, deviceCount):
            if p.get_device_info_by_host_api_device_index(0, i).get('maxInputChannels') > 0:
                    devices[i] = p.get_device_info_by_host_api_device_index(0, i).get('name')
    return devices


def print_all_devices() -> None:
    """
    Special command, prints all audio devices available
    """
    print('-- AUDIO devices:')
    devices = get_audio_devices()
    for key in devices:
        dev = devices[key]
        if isinstance(dev, bytes):
            dev = dev.decode("ascii", "replace")
        print('    id=%i - %s' % (key, dev))

def print_all_workers(server: str, port: int = 4443) -> None:
    """
    Special command, queries Mediator for Workers and prints them.
    """
    import pythonrecordingclient.mediatorLib as mlib
    import json
    assert server is not None, "No Mediator servername/address given"

    sock = mlib.create_connection(server, port)
    resp = mlib.recv_xml(sock)

    # Could be auth request from mediator
    if ("type", "auth") in resp.items() :
        raise NotImplementedError("Auth not implemented for this command yet") # TODO (reminder there already is mlib.do_auth for part)

    # Do challenge
    assert resp.tag == 'status' and ("type", "connect") in resp.items(), (
            f"Expected 'status' tag with 'connect' type in response from Mediator. Got: '{resp}'"
    )
    challenge = resp.attrib.get('description')
    assert challenge is not None, (
            "Expected 'description' attribute in response from Mediator."
    )
    solution = mlib.solve_challenge(challenge)

    # Request connected workers
    mlib.send(sock, f"<availablequeues sessionid=\"{solution}\"/>")
    workers = mlib.recv_msg(sock)
    sock.close()

    # Pretty print
    print(json.dumps(json.loads(workers), sort_keys=True, indent=4))

def get_title_from_api(hall_id, api: str) -> str:
    r = requests.get(args.api + '/events/?hall={}&offset_pre=30&offset_post=30'.format(args.hall))
    result = r.json()
    events = []
    for event in result['events']:
        start = datetime.strptime(event['start'], '%Y-%m-%dT%H:%M:%S')
        stop = datetime.strptime(event['stop'], '%Y-%m-%dT%H:%M:%S')
        event['cid'] = '{}_{}'.format(event['slug'], start.strftime('%y%m%d%H%M'))
        event['start'] = start.strftime('%Y-%m-%d %H:%M')
        event['stop'] = stop.strftime('%Y-%m-%d %H:%M')
        events.append(event)

    if len(events) == 0:
        print('No current events found')
        exit(0)

    for i, event in enumerate(events):
        print(str(i) + ')')
        print('slug:  ' + event['slug'])
        print('title: ' + event['title'])
        print('start: ' + event['start'])
        print('stop:  ' + event['stop'])
        print('\n')

    print('Which of the listed events should be started (enter number)?')
    num = eval(input('> '))
    while True:
        try:
            event = events[int(num)]
            break
        except (ValueError, IndexError):
            print('You Input could not be parsed. Try again:')
            num = eval(input('> '))
    return '{title}?N=L?T=en?SUB=en?S={slug}?D={start}?C={cid}'.format(**event)

def main(args: argparse.Namespace) -> None:
    global print_output, watchdog, logger

    configure_logging(getattr(logging, args.log.upper()))

    logger.warning("This script is deprecated and will be removed in favor of python_recording_client.py in a future version")
    time.sleep(3)

    if args.list:
        print_all_devices()
        exit(0)
    elif args.list_workers:
        print_all_workers(args.server, args.port)
        exit(0)

    if args.hall:
        title = get_title_from_api(args.hall, args.api)
    else:
        title = args.title

    if args.outputfile:
        ObjectStore.outfile = True

    print_output = args.printoutput
    if args.watchdog:
        raise NotImplementedError("Watchdog functionality has been unsupported")
        # if not print_output:
        #     raise Exception('must use printoutput when using watchdog')
        # watchdog = Watchdog(audio_rate=ObjectStore.RATE, session_name=title, input_fingerprint=args.fingerprint, email=args.watchdog)

    output_fingerprint = args.outfingerprint.split(',') if args.outfingerprint else []
    session = Session(
            name=title, description=args.desc, password=args.password,
            input_fingerprint=args.fingerprint, output_fingerprint=output_fingerprint,
            input_types=['audio'], logging=args.store, interactive=True,
            zoom_url=args.zoomurl, zoom_quick_cc=args.zoomquickcc,
            auth = Auth(args.user, args.passw, args.authserver) if args.user is not None else None
    )
    ObjectStore.session = session

    ObjectStore.setInputDevice(args.audiodevice)
    if args.audiochannel is not None:
        ObjectStore.setAudioChannelFilter(args.audiochannel)
    ObjectStore.CHUNKSIZE = args.chunksize

    print('SESSION START')
    session.start(args.server, args.port, http_tunnel=args.proxy)
    assert session.interface.socket.t is not None, RuntimeError("Session start failed.")
    session.interface.socket.t.join(99999)
    sys.exit(session.interface.exit_code)


# If we are not running interactively
if __name__ == '__main__':

    signal.signal(signal.SIGINT, stop_gracefully)

    parser = argparse.ArgumentParser(description='RecordingClient that streams live audio from a chosen audio interface')
    parser.add_argument('-S', '--server', help='server address')
    parser.add_argument('--list-workers', help="Alternative command. Query the Mediator for workers and print them", action='store_true')
    parser.add_argument('-H', '--hall', help='lecture hall id', default='')
    parser.add_argument('-A', '--api', help='URL to event API', default='https://lecture-translator.kit.edu')
    parser.add_argument('-p', '--port', type=int, help='port', default=4443)
    parser.add_argument('-t', '--title', help='title of the session', default='TestSession')
    parser.add_argument('-d', '--desc', help='description of the session', default='')
    parser.add_argument('-w', '--password', help='password of the session. This is not related to user/pass.', default='secret')
    parser.add_argument('-fi', '--fingerprint', help='fingerprint of the input stream', default='de-DE')
    parser.add_argument('-fo', '--outfingerprint', help='fingerprint of the output stream', default='')
    parser.add_argument('-po', '--printoutput', help='print the output stream', action='store_true')
    parser.add_argument('-s', '--store', help='store session in database', action='store_true')
    parser.add_argument('-r', '--record', help='record session in database (not implemented)', action='store_true')
    parser.add_argument('-c', '--chunksize', type=verify_chunk_size, help='size of chunks, that are sent to the mediator', default=1024)
    parser.add_argument('-l', '--log', help='logging level (Implemented but as of 92d559f very incomplete', default='info', choices=['critical', 'error', 'warn', 'info', 'debug'])
    parser.add_argument('-L', '--list', help='list audio available audio devices', action='store_true')
    parser.add_argument('-a', '--audiodevice', help='index of audio device to use', default='')
    parser.add_argument('-ch', '--audiochannel', help='index of audio channel to use (first channel = 1)', type=int, default=None)
    parser.add_argument('-o', '--outputfile', help='dump raw audio to file', action='store_true')
    parser.add_argument('--watchdog', help='email on missing text response (Not tested)', default=None, type=str)
    parser.add_argument('--zoomurl', help='Zoom video conference url for supplying subtitles', default=None, type=str)
    parser.add_argument('--zoomquickcc', help='Display Zoom subtitles in a fast, but not reliable way', action='store_true')
    parser.add_argument('--proxy', help="url to a http tunnel proxy 'https?://\w+(:\d+)?'", default=None, type=str)
    parser.add_argument('--user', help="Username for connecting with the authentication server. Needs to be used with --pass. Setting enables authentication.", default=None, type=str)
    parser.add_argument('--pass', help="Password for connecting with the authentication server. If not supplied you will be asked to enter it.", default=None, type=str, dest="passw")
    parser.add_argument('--authserver', help="Server used for logging in with --user and --pass", default="https://auth.lecture-translator.com", type=str)
    args = parser.parse_args()

    try:
        import pkg_resources
        version = pkg_resources.require("PythonRecordingClient")[0].version
        print("PythonRecordingClient Version {} for Python3".format(version))
    except:
        print("PythonRecordingClient Version ?")
    print()

    if not "help" in args:
        if not args.audiodevice and not args.list and not args.list_workers:
            print("Error: --list-workers, -a or -L flag missing. See -h")
            sys.exit(-1)
        main(args)
