#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import argparse
import logging
import os.path
import urllib.request, urllib.parse, urllib.error
from time import strptime

from pythonrecordingclient.helper import toDatetime
from pythonrecordingclient.session import Session, on_session_ready, on_receive
from pythonrecordingclient.config import Config


last = None
endTime = -1


def verify_chunk_size(value):
    try:
        val = int(value)
        assert(val > 0)
    except:
        val = value
        raise argparse.ArgumentTypeError('%s is an invalid positive int value' % value)
    return val


def verify_input_file(value):
    if not os.path.isfile(value):
        raise argparse.ArgumentTypeError('%s does not exist' % value)
    return value


def configure_logging(level):
    logger = logging.getLogger('prc')
    logger.setLevel(level)
    ch = logging.StreamHandler()
    ch.setLevel(level)
    formatter = logging.Formatter('%(name)s - %(levelname)s - %(message)s')
    ch.setFormatter(formatter)
    logger.addHandler(ch)


@on_session_ready
def session_ready(session):
    global endTime
    with open(args.file, 'r') as f:
        line = f.readline()
        while line:
            session.send_text(urllib.parse.quote(line + '<br><br>'))
            line = f.readline()
        session.send_text(urllib.parse.quote('end'))
        endTime = session.interface.current_ts


@on_receive
def receive(session, msg):
    global last, endTime
    if last is not None:
        start = strptime(msg.get('start'), '%d/%m/%y-%H:%M:%S.%f')
        if endTime != -1 and start >= endTime:
            session.stop()
        if last.get('start') != msg.get('start'):
            print(last.get('fingerprint') + ':', urllib.parse.unquote(last.find('text').text))
    last = msg


def main(args):
        configure_logging(getattr(logging, args.log.upper()))
        Config.set(BUFFER_CHUNKS=False, CORRECT_TIME=False)
        session = Session(name=args.title, description=args.desc, password=args.password, input_fingerprint=args.fingerprint, input_type=args.type,output_fingerprint=args.outputfingerprint,logging=args.store)
        session.start(args.server, args.port)
        session.sender.t.join(99999)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='RecordingClient that streams text from a text file')
    parser.add_argument('server', help='server address')
    parser.add_argument('file', type=verify_input_file, help='input file')
    parser.add_argument('-p', '--port', type=int, help='port', default=4443)
    parser.add_argument('-t', '--title', help='title of the session', default='TestSession')
    parser.add_argument('-d', '--desc', help='description of the session', default='')
    parser.add_argument('-w', '--password', help='password of the session', default='secret')
    parser.add_argument('-f', '--fingerprint', help='fingerprint of the input stream', default='de-DE')
    parser.add_argument('-o', '--outputfingerprint', help='fingerprint of the output stream', action='append',default=[])
    parser.add_argument('-y', '--type', help='type of the input stream', default='text')
    parser.add_argument('-s', '--store', help='store session in database', action='store_true')
    parser.add_argument('-l', '--log', help='logging level', default='info', choices=['critical', 'error', 'warn', 'info', 'debug'])
    args = parser.parse_args()
    main(args)
