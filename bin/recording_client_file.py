#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import argparse
import logging
import os
import signal
import sys
import urllib.request, urllib.parse, urllib.error
from datetime import datetime
from threading import Thread
from time import sleep

from pythonrecordingclient.session import Session, on_session_ready, on_receive
from pythonrecordingclient.mediatorLib import Auth
from pythonrecordingclient.helper import extract_pcm_audio

logger = logging.getLogger('prc')
print_output = False
chunksize = 1024
current_ms = 0
total_ms = 0
filepath = ''
session = None


def stop_gracefully(signal, frame):
    session.stop()


def verify_chunk_size(value):
    try:
        val = int(value)
        assert(val > 0)
    except:
        val = value
        raise argparse.ArgumentTypeError('%s is an invalid positive int value' % value)
    return val


def verify_input_file(value):
    if not value.startswith('http://') and not value.startswith('https://') and not os.path.isfile(value):
        raise argparse.ArgumentTypeError('%s does not exist' % value)
    return value


def configure_logging(level):
    logger.setLevel(level)
    logger.propagate = False
    ch = logging.StreamHandler(sys.stdout)
    ch.setLevel(level)
    formatter = logging.Formatter(fmt='[{asctime}][{levelname:^8}]: {message}', datefmt='%H:%M:%S', style='{')
    ch.setFormatter(formatter)
    logger.addHandler(ch)


def check_end_loop(session):
    local_current_ms = current_ms
    idle = 0
    while True:
        if local_current_ms != current_ms:
            idle = 0
            local_current_ms = current_ms
        else:
            idle += 1
        if idle > min((total_ms - local_current_ms) // 1000, 900):
            session.stop()
            break
        else:
            sleep(1)


@on_receive
def session_receive(session, el):
    global current_ms
    current_ms = int(el.get('startoffset'))
    if not print_output or el.find('text').text is None:
        return
    start = el.get('start')
    stop = el.get('stop')
    creator = el.get('creator')
    fingerprint = el.get('fingerprint')
    text = urllib.parse.unquote(el.find('text').text)
    print(' | '.join([creator, str(session.id), fingerprint, start, stop]) + ': ' + text)


@on_session_ready
def session_ready(session):
    print('READY')
    t1 = Thread(target=send_audio, args=(session, ))
    t1.daemon = True
    t1.start()
    t2 = Thread(target=check_end_loop, args=(session,))
    t2.daemon = True
    t2.start()


def send_audio(session):
    seconds_per_chunk = chunksize / 32000.0
    now = datetime.now()
    seconds = 0
    chunks = 0
    with open(filepath, 'rb') as f:
        b = f.read(44)
        b = f.read(chunksize)
        while b != '':
            delta = seconds_per_chunk * chunks - seconds
            if delta > 0:
                sleep(delta)
            session.send_audio(b)
            b = f.read(chunksize)
            seconds = (datetime.now() - now).total_seconds()
            chunks += 1


def download_and_convert(url):
    video_file = os.path.join('/tmp', os.path.basename(url))
    video_wo_ext, _ = os.path.splitext(video_file)
    audio_file = video_wo_ext + '.wav'
    opener = urllib.request.URLopener()
    try:
        opener.retrieve(url, video_file)
    except:
        return
    extract_pcm_audio(video_file, audio_file)
    os.remove(video_file)
    return audio_file

def print_all_workers(server: str, port: int = 4443) -> None:
    """
    Special command, queries Mediator for Workers and prints them.
    """
    import pythonrecordingclient.mediatorLib as mlib
    import json
    assert server is not None, "No Mediator servername/address given"

    sock = mlib.create_connection(server, port)
    resp = mlib.recv_xml(sock)

    # Could be auth request from mediator
    if ("type", "auth") in resp.items() :
        raise NotImplementedError("Auth not implemented yet") # TODO (reminder there already is mlib.do_auth for part)

    # Do challenge
    assert resp.tag == 'status' and ("type", "connect") in resp.items(), (
            f"Expected 'status' tag with 'connect' type in response from Mediator. Got: '{resp}'"
    )
    challenge = resp.attrib.get('description')
    assert challenge is not None, (
            "Expected 'description' attribute in response from Mediator."
    )
    solution = mlib.solve_challenge(challenge)

    # Request connected workers
    mlib.send(sock, f"<availablequeues sessionid=\"{solution}\"/>")
    workers = mlib.recv_msg(sock)
    sock.close()

    # Pretty print
    print(json.dumps(json.loads(workers), sort_keys=True, indent=4))


def main(args):
    global print_output
    global chunksize
    global filepath
    global total_ms
    global session

    configure_logging(getattr(logging, args.log.upper()))

    remove_source_file = False
    print_output = args.printoutput
    chunksize = args.chunksize
    filepath = args.filepath

    if args.list_workers:
        print_all_workers(args.server, args.port)
        exit(0)

    if filepath.startswith('http://') or filepath.startswith('https://'):
        filepath = download_and_convert(filepath)
        remove_source_file = True

    total_ms = (os.path.getsize(filepath) - 44) // 32

    output_fingerprint = args.outfingerprint.split(',') if args.outfingerprint else [args.fingerprint.split('-')[0]]
    session = Session(name=args.title, description=args.desc, password=args.password,
                  input_fingerprint=args.fingerprint, output_fingerprint=output_fingerprint,
                  input_types=['audio'], logging=args.store,
                  zoom_url=args.zoomurl, zoom_quick_cc=args.zoomquickcc,
                  auth = Auth(args.user, args.passw, args.authserver))

    session.start(args.server, args.port, http_tunnel=args.proxy)
    session.interface.socket.t.join(99999)
    if remove_source_file:
        os.remove(filepath)
    sys.exit(session.interface.exit_code)


# If we are not running interactively
if __name__ == '__main__':

    signal.signal(signal.SIGINT, stop_gracefully)

    parser = argparse.ArgumentParser(description='RecordingClient that streams the content of an audio file')
    parser.add_argument('--list-workers', help="Alternative command. Query the Mediator for workers and print them", action='store_true')
    parser.add_argument('-S', '--server', help='server address')
    parser.add_argument('-p', '--port', type=int, help='port', default=4443)
    parser.add_argument('-t', '--title', help='title of the session', default='TestSession')
    parser.add_argument('-d', '--desc', help='description of the session', default='')
    parser.add_argument('-w', '--password', help='password of the session', default='secret')
    parser.add_argument('-fi', '--fingerprint', help='fingerprint of the input stream', default='de-DE')
    parser.add_argument('-fo', '--outfingerprint', help='fingerprint of the output stream', default='')
    parser.add_argument('-po', '--printoutput', help='print the output stream', action='store_true')
    parser.add_argument('-s', '--store', help='store session in database', action='store_true')
    parser.add_argument('-c', '--chunksize', type=verify_chunk_size, help='size of chunks, that are sent to the mediator', default=1024)
    parser.add_argument('-l', '--log', help='logging level', default='info', choices=['critical', 'error', 'warn', 'info', 'debug'])
    parser.add_argument('-f', '--filepath', help='path to wav file or url (mp4 file on web)', type=verify_input_file, default='')
    parser.add_argument('--zoomurl', help='Zoom video conference url for supplying subtitles', default=None, type=str)
    parser.add_argument('--zoomquickcc', help='Display Zoom subtitles in a fast, but not reliable way', action='store_true')
    parser.add_argument('--proxy', help="url to a http tunnel proxy 'https?://\w+(:\d+)?'", default=None, type=str)
    parser.add_argument('--user', help="Username for connecting with the authentication server. Needs to be used with --pass. Setting enables authentication.", default=None, type=str)
    parser.add_argument('--pass', help="Password for connecting with the authentication server. If not supplied you will be asked to enter it.", default=None, type=str, dest="passw")
    parser.add_argument('--authserver', help="Server used for logging in with --user and --pass", default="https://auth.lecture-translator.com", type=str)

    args = parser.parse_args()
    main(args)
