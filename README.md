# Python RecordingClient

The Python RecordingClient connects as RecordingClient to a Mediator and streams audio as session input.

It consists out of a library and some python scripts for the command line using the aforementioned library.

If you are an ordinary user you are probably interested in the command line scripts.

## System prerequisites

- portaudio
- ffmpeg (only when using `recording_client_file.py` with url as input)

## Install

    pip install git+https://bitbucket.org/isl_kit/pythonrecordingclient.git


## Usage (Library)

### Start a session

    from pythonrecordingclient.session import Session

    session = Session(
        name='TestSession',
        description='this is a test session',
        password='secret',
        input_fingerprint='de-DE-lecture_KIT',
        input_types=['audio'],
        output_fingerprint=['de', 'en'],
        logging=True
    )
    session.start('i13srv30.ira.uka.de', 4443)

### Get session status

To determine when a session is ready for streaming data, you can either check the `ready` property:

    is_session_ready = session.ready

Or you can use the `on_session_ready` event handler:

    from pythonrecordingclient.session import on_session_ready

    @on_session_ready
    def session_ready(session):
        # session.ready == True
        # start streaming data ....

### Streaming data

#### Example: Sending audio from file

    chunksize = 1024
    with open('/path/to/audio.wav', 'rb') as f:
        b = f.read(44) # strip off wave header
        b = f.read(chunksize)
        while b != '':
            session.send_audio(b)
            b = f.read(chunksize)

#### Example: Sending text

    global endTime
    with open('/path/to/file.txt', 'r') as f:
        line = f.readline()
        while line:
            session.send_text(urllib.quote(line + '<br><br>'))
            line = f.readline()

### Get received data

When creating a session, you can use the keyword argument `output_fingerprint` to subscribe to one or multiple output streams. To access the data from this streams, you should use the `on_receive` event handler:

    from pythonrecordingclient.session import on_receive

    @on_receive
    def session_receive(session, el):
        text = el.find('text').text
        start = el.get('start')
        stop = el.get('stop')
        creator = el.get('creator')
        fingerprint = el.get('fingerprint')
        if text is not None:
            print urllib.unquote(text).decode('utf8')

### Stop session

    session.stop()

## Command line tools

The three recording_client_live_\*.py scripts showcased below are **deprecated** and have been reimplemented in
python_recording_client.py as backends. (Note: raw audio without headers as used with file.py is currently not
supported with the new script. This will be fixed before the old scripts will be removed.

See -h for info on how to use the tool.

Old explanation of the tools:

Python RecordingClient comes with three different command line tools, that wrap aroud the core library and enable you to start sessions from the command line that stream data from a local wave file or a webhosted audio/video file (recording_client_file.py), from an audio interface (recording_client_live.py) or from a text file (recording_client_text.py).

### recording_client_file.py

This tool streams audio from a wav file or from a webhosted audio or video file, which will then be downloaded and converted to a wave file before streaming. The streaming is done in real-time.

#### Example usage

    recording_client_file.py \
        -S i13srv30 \
        -t TestSession \
        -fi de-DE-lecture_KIT \
        -fo en \
        -po \
        -s \
        -f /path/to/audio.wav

### recording_client_live.py

This tool streams audio in real-time from a selected audio interface.

#### Example usage
##### 1. Over all usage

    # print all available audio interfaces
    recording_client_live.py -L 1

    # start session with audio interface a
    recording_client_live.py \
        -S i13srv30 \
        -t TestSession \
        -fi de-DE-lecture_KIT \
        -fo en \
        -po \
        -s \
        -a 2

##### 2. Zoom closed captioning service

    # print all available audio interfaces
    recording_client_live.py -L 1

    # start session with audio interface a
    recording_client_live.py \
        -S i13srv30 \
        -t TestSession \
        -fi de-DE-lecture_KIT \
        -fo en \
        -po \
        -s \
        -a 2
        --zoomurl https://wmcapi.zoom.us/closedcaption?id=200610693&ns=GZHkEA==&expire=86400&spparams=id%2Cns%2Cexpire&signature=nYtXJqRKCW
If you want to pass the URL for closed captioning over HTTP via the title of the Session,
you *need* to make that you encode the URL.
Else it will result in an unhandled Exeption. 
To be sure use the ```--zoomurl``` parameter to the Recording Client as shown in the example above.

    # print all available audio interfaces
    recording_client_live.py -L 1

    # start session with audio interface a
    recording_client_live.py \
        -S i13srv30 \
        -t TestSession \
        -fi de-DE-lecture_KIT \
        -fo en \
        -po \
        -s \
        -a 2 \
        --zoomurl https://wmcapi.zoom.us/closedcaption?id=200610693&ns=GZHkEA==&expire=86400&spparams=id%2Cns%2Cexpire&signature=nYtXJqRKCW \
        --zoomquickcc
If you want fast subtitles, make sure to include ```--zoomquickcc```.


### recording_client_text.py

This tool needs review. No documentation available. Will be removed soon.
