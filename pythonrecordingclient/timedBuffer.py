#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from builtins import object
from datetime import datetime

from .helper import toStamp, toDatetime

class TimedBuffer(object):

    def __init__(self, size, accurate_time=True):
        self.size = size
        self.data = ""
        self.times = []
        self.accurate_time = accurate_time

    def put(self, chunk):
        if self.accurate_time:
            n = toStamp(datetime.now())
            self.times.append([n, len(self.data)])
        self.data += chunk

    def get(self):
        if len(self.data) >= self.size:
            chunk = self.data[:self.size]
            self.data = self.data[self.size:]
            if self.accurate_time:
                ts1, pos1 = self.times[0]
                if pos1 < 0:
                    if len(self.times) > 1:
                        ts2, pos2 = self.times[1]
                        ts = int(ts1 - 1.0 * (ts2 - ts1) / (pos2 - pos1) * pos1)
                    else:
                        ts = ts1 - pos1 * 1000000.0 // 32000
                else:
                    ts = ts1
                dt = toDatetime(ts)
                newt = []
                cc = True
                self.times.reverse()
                for idx, t in enumerate(reversed(self.times)):
                    t[1] -= self.size
                    if t[1] > 0 or idx == 0:
                        newt.insert(0, t)
                    elif cc:
                        cc = False
                        newt.insert(0, t)
                self.times = newt
            else:
                ts = toStamp(datetime.now())
            return (chunk, ts)
        return (None, None)
