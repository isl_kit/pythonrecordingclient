# -*- coding: utf-8 -*-
# This file is old and needs to get reworked anyway, so disable checking
# type: ignore

from builtins import str
from builtins import object
from timeit import default_timer as timer
import logging
import smtplib
import threading
import time
import os
import socket
import datetime
import numpy as np
from email.mime.application import MIMEApplication
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.utils import COMMASPACE, formatdate
import json

logger = logging.getLogger("prc")


def rms(arr):
    return np.sqrt(np.mean(np.square(arr.astype("float32"))))


# store last [window] elements of array (inefficient)
class MovingWindow(object):
    def __init__(self, window):
        self.window = window
        self.arr = None

    def append(self, arr):
        if self.arr is None:
            self.arr = arr
        else:
            self.arr = np.append(self.arr, arr)
        self.arr = self.arr[-self.window:]

    def get_window(self):
        return self.arr


class WatchdogState(object):
    def __init__(self):
        self.last_seen_text = None
        self.last_text = []
        self.last_triggered = None


if "smtp_config" not in os.environ:
    print("environment variable smtp_config not set, should look like this:")
    print("""
    {
        "server": "smtp.gmail.com",
        "port": 587,
        "from": "developer@karlsruhe-it-solutions.com",
        "login": {
            "username": "abc",
            "password": "abc"
        }
    }""")


class Watchdog(object):
    trigger_ratelimit_s = 120
    text_missing_threshold_s = 40  # number of seconds that no text has been received to wait before sending email
    audio_power_threshold = 0.0  # minimum average audio level over that period
    smtp_config = json.loads(os.environ["smtp_config"])

    def __init__(self, session_name, input_fingerprint, email, audio_rate):
        self.session_name = session_name
        self.input_fingerprint = input_fingerprint
        self.email = email
        self.audio_rate = audio_rate
        self.fingerprints = {}
        self.audio_window = MovingWindow(audio_rate * self.text_missing_threshold_s)
        self.thread = threading.Thread(name="Watchdog {}".format(session_name), target=self.loop, args=())
        self.thread.daemon = True
        self.thread.start()

    def received_text(self, fingerprint, text):
        target = self.fingerprints.get(fingerprint) or self.fingerprints.setdefault(fingerprint, WatchdogState())
        target.last_seen_text = timer()
        target.last_text.append(text)
        target.last_text = target.last_text[-5:]

    def sent_audio(self, audio_data):
        self.audio_window.append(audio_data)

    def loop(self):
        while True:
            time.sleep(1)
            for fingerprint, target in self.fingerprints.items():
                # logger.info('watchdog: {}'.format(timer() - self.last_seen_text if self.last_seen_text else 0))
                if target.last_seen_text is not None and timer() - target.last_seen_text > self.text_missing_threshold_s:
                    self.trigger(fingerprint)

    def trigger(self, target_fingerprint):
        target = self.fingerprints[target_fingerprint]
        delay = timer() - target.last_seen_text if target.last_seen_text else float('inf')
        if target.last_triggered is not None and timer() - target.last_triggered < self.trigger_ratelimit_s:
            # logger.info('watchdog: trigger ratelimited')
            return
        logger.info('watchdog: no text for {} received for {} s'.format(target_fingerprint, delay))
        target.last_triggered = timer()
        session_name = self.session_name
        input_fingerprint = self.input_fingerprint
        # python2 can't properly handle time zones for ISO format. whatever
        time = datetime.datetime.now().isoformat("T")
        hostname = socket.gethostname()
        avg_power = rms(self.audio_window.get_window()) // 2**15
        if avg_power < self.audio_power_threshold:
            logger.info('watchdog: ignoring no text because audio silent')
            return
        text_missing_threshold_s = self.text_missing_threshold_s
        pid = os.getpid()
        last_text = "\n".join(target.last_text)
        try:
            with open('/proc/{}/cmdline'.format(pid), "r") as f:
                cmdline = str(f.read()[:-1].split(b'\0'))
        except OSError as err:
            cmdline = 'err: ' + str(err)
        logger.warn('watchdog: triggering email')
        subject = """
[recordingclient] Warning: session {session_name}: {input_fingerprint}→{target_fingerprint} has not received text for {delay} seconds.
        """.format(**locals()).strip()

        content = """
Warning: session {session_name}: {input_fingerprint}→{target_fingerprint} has not received text for {delay} seconds.

Sent automatically by pythorecordingclient watchdog.

Host: {hostname}
Time: {time}
PID: {pid}
Avg power over last {text_missing_threshold_s} seconds: {avg_power}
cmdline: {cmdline}
Last text:
{last_text}
        """.format(**locals()).strip()
        msg = MIMEMultipart()
        conf = self.smtp_config
        to = self.email
        msg['From'] = conf["from"]
        msg['To'] = to
        msg['Date'] = formatdate(localtime=True)
        msg['Subject'] = subject
        msg.attach(MIMEText(content))
        try:
            fname = 'last30seconds.ogg'
            part = MIMEApplication(audio_to_ogg(self.audio_window.get_window(), sample_rate=self.audio_rate), Name=fname)
            part['Content-Disposition'] = 'attachment; filename="%s"' % fname
            msg.attach(part)
        except Exception as e:
            logger.error('audio attachment failed')
            print(e)
        try:
            smtp = smtplib.SMTP(conf["server"], conf["port"])
            smtp.starttls()
            # smtp.set_debuglevel(True)
            login = conf.get("login")
            if login is not None:
                smtp.login(login["username"], login["password"])
            smtp.sendmail(conf["from"], to, msg.as_string())
            smtp.close()
        except Exception as e:
            logger.error('Sendmail failed')
            print(e)


def audio_to_ogg(audio_data, sample_rate):
    import subprocess
    p = subprocess.Popen(['ffmpeg', "-hide_banner", "-loglevel", "error", "-f", "s16le", "-ar", str(sample_rate), "-i", "-", "-c:a", "libopus", "-b:a", "20k", "-f", "ogg", "-"], stdin=subprocess.PIPE, stdout=subprocess.PIPE)
    stdout, stderr = p.communicate(audio_data.tostring())
    return stdout
