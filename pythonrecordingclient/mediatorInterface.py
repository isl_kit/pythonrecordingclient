#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from typing import TYPE_CHECKING, Callable, Dict, List, Any, Optional, Union
from builtins import str, object
import logging
import xml.etree.ElementTree as ET
import time
from base64 import b64encode, b64decode
from datetime import datetime, timedelta

from .helper import objToXml
from .mediatorLib import do_auth
from .config import Config
from .xmlSocketServer import XMLSocket

if TYPE_CHECKING:
    from .session import Session

logger = logging.getLogger("prc")


class MediatorInterface(object):

    def __init__(self,
            host_address: str,
            port: int,
            session: 'Session',
            close_handler: Callable = None,
            ready_handler: Callable = None,
            receive_handler: Callable = None,
            http_tunnel: Optional[str] = None
    ):
        self.state = "idle"

        self.socket = XMLSocket(
                host_address, port,
                message_handler=self._handle_message, close_handler=self._close_handler,
                interactive=session.interactive, http_tunnel=http_tunnel
        )

        self._BYTES_PER_SAMPLE = Config.BITS_PER_SAMPLE // 8
        self._BIT_RATE = Config.SAMPLE_RATE * Config.BITS_PER_SAMPLE
        self._BYTE_RATE = Config.SAMPLE_RATE * self._BYTES_PER_SAMPLE

        self.number_of_inputs = 0
        self.number_of_outputs = 0
        self.current_ts: Optional[int] = None
        self.session = session
        self.syncd = False
        self.buf: Dict[str, List[Any]] = { # TODO correct typing
            "audio": [],
            "video": []
        }
        self.close_handler = close_handler
        self.ready_handler = ready_handler
        self.receive_handler = receive_handler
        self.exit_code = 0

    def connect(self) -> None:
        self.socket.connect()

    def close(self, exit_code:int = 0) -> None:
        if self.exit_code == 0:
            self.exit_code = exit_code
        self.socket.close()

    def send_audio(self, data: bytes) -> None:
        length = len(data)
        start, stop = self._get_times(length / self._BYTE_RATE)
        if "audio" not in self.session.input_types or self.state != "running":
            return
        if "video" in self.session.input_types and not self.syncd:
            self.buf["audio"].append([data, start, stop])
            self._try_sync()
            return
        self._send_audio(data, start, stop)

    def send_video(self, data) -> None:
        start = datetime.now()
        if "video" not in self.session.input_types or self.state != "running":
            return
        if not self.syncd:
            self.buf["video"].append([data, start])
            return
        self._send_video(data, start)

    def send_text(self, data) -> None:
        if "text" not in self.session.input_types or self.state != "running":
            return
        self._send_text(data)

    def _send_audio(self, chunk: bytes, start, stop) -> None:
        length = len(chunk)
        start = start.strftime("%d/%m/%y-%H:%M:%S.%f")[:-3]
        stop = stop.strftime("%d/%m/%y-%H:%M:%S.%f")[:-3]
        samples = length // self._BYTES_PER_SAMPLE
        xmlMsg = "<data type=\"audio\" fingerprint=\"" + self.session.input_fingerprint + "\" creator=\"PythonClient\" start=\"" + start + "\" stop=\"" + stop + "\"><audio bits=\"" + str(Config.BITS_PER_SAMPLE) + "\" srate=\"" + str(Config.SAMPLE_RATE) + "\" channels=\"1\" brate=\"" + str(self._BIT_RATE) + "\" samples=\"" + str(samples) + "\" codec=\"RPCM\" encoding=\"base64\" format=\"raw\">{}</audio></data>"
        self._send_binary_xml(xmlMsg, chunk)

    def _send_video(self, chunk, start):
        start = start.strftime("%d/%m/%y-%H:%M:%S.%f")[:-3]
        xmlMsg = "<data type=\"video\" fingerprint=\"" + self.session.input_fingerprint + "\" creator=\"PythonClient\" start=\"" + start + "\"><video encoding=\"base64\" format=\"h264\">{}</video></data>"
        self._send_binary_xml(xmlMsg, chunk)

    def _send_text(self, text):
        start, stop = self._get_times(self.session.text_msg_delta)
        xmlMsg = "<data type=\"text\" fingerprint=\"" + self.session.input_fingerprint + "\" creator=\"PythonClient\" start=\"" + start + "\" stop=\"" + stop + "\"><text>{}</text></data>"
        self._send_data_xml(xmlMsg, text, text)

    def _try_sync(self):
        if len(self.buf["video"]) == 0 or len(self.buf["audio"]) == 0:
            return
        video_start = self.buf["video"][0][1]
        drop = 0
        success = False
        for audio in self.buf["audio"]:
            audio_chunk, audio_start, audio_stop = audio
            if audio_stop > video_start:
                diff = round((audio_start - video_start).total_seconds() * Config.SAMPLE_RATE) / Config.SAMPLE_RATE
                diff_bytes = int(diff * self._BYTE_RATE)
                audio_start -= timedelta(seconds=diff)
                if diff_bytes > 0:
                    audio_chunk = bytearray(diff_bytes) + audio_chunk
                else:
                    audio_chunk = audio_chunk[diff_bytes:]
                success = True
                audio[0] = audio_chunk
                audio[1] = audio_start
                break
            drop += 1
        self.buf["audio"] = self.buf["audio"][drop:]
        if success:
            self._send_buffer()

    def _send_buffer(self):
        for audio_chunk, audio_start, audio_stop in self.buf["audio"]:
            self._send_audio(audio_chunk, audio_start, audio_stop)
        for video_chunk, video_start in self.buf["video"]:
            self._send_video(video_chunk, video_start)
        self.syncd = True

    def _get_times(self, delta):
        if not self.current_ts:
            self.current_ts = datetime.now()
        start_ts = self.current_ts
        stop_ts = start_ts + timedelta(seconds=delta)
        self.current_ts = stop_ts
        return (start_ts, stop_ts)

    def _handle_start_session(self):
        if self.state != "idle":
            return
        self.state = "session_creating"
        self.number_of_inputs = 0
        self.number_of_outputs = 0
        self.curren_ts = None
        xmlMsg = objToXml("flow", {
            "sessionid": self.session.id,
            "logging": "true" if self.session.logging else "false",
            "password": self.session.password,
            "sessioninfos": {
                "sessioninfo": {
                    "language": self.session.input_fingerprint,
                    "name": self.session.name,
                    "description": self.session.description
                }
            }
        })
        self._send_xml(xmlMsg)

    def _handle_set_input(self):
        if self.state != "session_creating":
            return
        self.state = "input_creating"
        xmlMsgs = []
        for input_type in self.session.input_types:
            xmlMsgs.append(objToXml("inputnode", {
                "type": input_type,
                "fingerprint": self.session.input_fingerprint,
                "streamid": "slides" if input_type == "video" else "speech"
            }))
        for xmlMsg in xmlMsgs:
            self._send_xml(xmlMsg)

    def _handle_set_output(self) -> None:
        if self.state != "input_creating":
            return
        self.state = "output_creating"
        for fingerprint in (fo.rsplit(":",1) for fo in self.session.output_fingerprint):
            lang = fingerprint[0]
            type = fingerprint[1] if len(fingerprint) > 1 else "text"
            xmlMsg = objToXml("stream", {
                "type": type,
                "fingerprint": lang,
                "streamid": "speech"
            })
            self._send_xml(xmlMsg)

    def _handle_announce(self):
        if self.state != "input_creating" and self.state != "output_creating":
            return
        xmlMsg = objToXml("announce", {})
        self._send_xml(xmlMsg)
        self.state = "running"
        if self.ready_handler:
            self.ready_handler()

    def _handle_auth_request(self) -> None:

        auth = self.session.auth
        if auth is None:
            logger.error("Mediator requires authentication but you did not specify a login.")
            self.session.stop()
            return

        assert self.socket.socket is not None
        if not do_auth(self.socket.socket, auth, check_required=False):
            self.session.stop()
            return


    def _send_xml(self, msg):
        logger.info(" >>> " + msg)
        self.socket.sendXML(msg)

    def _send_binary_xml(self, msg, data):
        self._send_data_xml(msg, b64encode(data), "<<binary/{}>>".format(len(data)))

    def _send_data_xml(self,
            msg: str,
            data: Union[str, bytes],
            print_data: Optional[str]
    ) -> None:
        if isinstance(data, bytes):
            data = data.decode("ascii")
        logger.debug(" >>> " + msg.format(print_data))
        self.socket.sendXML(msg.format(data))

    def _handle_message(self, msg: str) -> None:
        try:
            el = ET.fromstring(msg)
        except ET.ParseError:
            logger.error(f"Could not parse xml message:\n===START OF MSG===\n'{msg}'\n===END OF MSG===")
            self.session.stop()
            return

        if el.tag == "status":
            logger.info(" <<< " + msg)
            desc = el.get("description")
            msgtype = el.get("type")
            if msgtype == "auth":
                self._handle_auth_request()
            elif msgtype == "connect":
                assert desc is not None, "The Mediator is expected to return a description containing the encrypted id."
                session_id = int((int(desc) * 194510094) % 1999999943) # "Decrypt" session id
                self.session.id = session_id
                self._handle_start_session()
            elif msgtype == "ok" and desc == "flow created":
                self._handle_set_input()
            elif msgtype == "ok" and not desc:
                self.number_of_inputs += 1
                if self.number_of_inputs == len(self.session.input_types):
                    if len(self.session.output_fingerprint) > 0:
                        self._handle_set_output()
                    else:
                        self._handle_announce()
            elif msgtype == "ok" and desc:
                self.number_of_outputs += 1
                if self.number_of_outputs == len(self.session.output_fingerprint):
                    self._handle_announce()
            elif msgtype == "error":
                code = el.get("code")
                description = el.get("description")
                logger.error("could not connect to output stream! Ending...") # TODO error can now have different reasons
                logger.error(f"Mediator error code: {code} Description: {description}")
                self.close(3)
        elif el.tag == "data":
            if self.receive_handler:
                self.receive_handler(el)

    def _close_handler(self, exit_code: int) -> None:
        if self.exit_code == 0:
            self.exit_code = exit_code
        if self.close_handler:
            self.close_handler(self.exit_code)
