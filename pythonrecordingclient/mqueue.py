#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from builtins import object
from multiprocessing import Queue


class MQueue(object):

    def __init__(self, q=None):
        if q is None:
            self.q = Queue()
        else:
            self.q = q
        self.closed = False

    def put(self, data):
        if not self.closed:
            self.q.put(data)

    def get(self):
        if not self.closed:
            try:
                return self.q.get()
            except:
                return None
        else:
            return None

    def close(self):
        if self.closed:
            return
        self.closed = True
        self.q.put(None)
        self.q.cancel_join_thread()
