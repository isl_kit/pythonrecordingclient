#!/usr/bin/env python3
# -*- coding: utf-8 -*-


from builtins import object
class Config(object):

    SAMPLE_RATE = 16000
    BITS_PER_SAMPLE = 16

    CHUNK_SIZE = 16000
    BUFFER_CHUNKS = True
    CORRECT_TIME = True

    @staticmethod 
    def set(**kwargs):
        for k,v in Config.__dict__.items():
            if k.startswith("__") or isinstance(v, staticmethod):
                continue
            if k in kwargs:
                setattr(Config, k, kwargs[k])

    @staticmethod 
    def dict(**kwargs):
        return {k:v for k,v in Config.__dict__.items() if not k.startswith("__") and not isinstance(v, staticmethod)}
