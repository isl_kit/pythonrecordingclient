#!/usr/bin/env python3


from typing             import Any, Dict, Optional, cast, List
from abc                import ABCMeta, abstractmethod

import logging
import numpy as np
import sys
import time

from pythonrecordingclient.mediatorLib	import MediatorAudioFormat
from pythonrecordingclient.helper	import BugException

logger = logging.getLogger('prc')
# Letfover from old feature. Not currently used
watchdog = None

# TODO make these optional
try:
    import pyaudio #typing: ignore
except:
    logger.warning("Could not import pyaudio libary. Audio input via portaudio (system) audio will not work.")

import subprocess

# TODO chunksize (and rate) setting has not been thought through

class BaseAdapter(metaclass=ABCMeta):
    default_mediator_format = MediatorAudioFormat(rate = 16000, chunksize = 1024, format = pyaudio.paInt16)

    def __init__(self, format: Any, chunksize: int = 1024) -> None:
        self.rate = 16000
        self.chunksize = chunksize
        self.format = format
        self.channel_count = 1
        self.chosen_channel: Optional[int] = None

    def available(self) -> bool:
        """
        Should return if the backend is available and print an error message
        if not. Called before setting input
        """
        return True

    @abstractmethod
    def get_stream(self, **kwargs) -> Any:
        """
        Should return the stream for reading
        """
        pass

    @abstractmethod
    def read(self, chunksize: Optional[int] = None) -> bytes:
        """
        Should return a chunk of bytes from the audio device.
        If chunksize is None, use internal value or any size.
        Might block depending on device.
        chunksize might be ignored
        """
        pass

    def chunk_modify(self, chunk: bytes) -> bytes:
        """
        Allows modifying of the chunk
        """
        return chunk

    @abstractmethod
    def cleanup(self) -> None:
        """
        Should be called after the session was closed
        """
        pass

    @abstractmethod
    def set_input(self, input: Any) -> None:
        """
        Should be called to set an input, which can be for example an id, string etc
        """
        pass



class FfmpegStream(BaseAdapter):
    def __init__(self, **kwargs) -> None:
        """
            Requires named parameter chunksize, pre_input and post_output,
            volume, repeat_input
        """
        if "pre_input" not in kwargs or kwargs["pre_input"] is None:
            kwargs["pre_input"] = ""
        if "post_input" not in kwargs or kwargs["post_input"] is None:
            kwargs["post_input"] = ""
        if "chunksize" not in kwargs:
            kwargs["chunksize"] = None
        self._process: Optional[subprocess.Popen] = None
        self.url: Optional[str] = None
        self.pre_opt: List[str] = kwargs["pre_input"].split()
        self.post_opt: List[str] = kwargs["post_input"].split()
        self.volume: float = kwargs["volume"]
        self.repeat_input: bool = kwargs["repeat_input"]
        super().__init__(format=None, chunksize=kwargs["chunksize"])

    def available(self) -> bool:
        import shutil
        if shutil.which('ffmpeg') is None:
            logger.error('ffmpeg could not be found. Is it installed?')
            return False
        else:
            return True

    def get_stream(self, **kwargs) -> Any:
        if self.url is None:
            raise BugException("Input file/url cannot be None")
        if self._process is None:
            args: List[str] = [
                "ffmpeg",
                # be less verbose (but still show stats)
                "-hide_banner", "-loglevel", "warning", "-stats",
            ]
            if len(self.pre_opt) > 0:
                args += self.pre_opt
            args += [
                # ignore video tracks
                #"-re",
                #"-rtsp_transport", "tcp",
                "-vn",
                "-i", self.url,
                *self.post_opt
            ]
            if len(self.post_opt) > 0:
                args+= self.post_opt
            args += [
                # use the first audio channel
                "-map", "0:a",
                "-ac", "1",
                # adjust volume
                "-filter:a", "volume={volume}".format(volume=self.volume),
                # convert to 16kHz signed little endian, one audio channel only
                "-f", "s16le",  "-ar", str(self.rate), "-c:a",  "pcm_s16le",
                "-"
            ]
            logger.debug(args)
            self._process = subprocess.Popen(args, stdin=subprocess.PIPE,
                    stdout=subprocess.PIPE)
        return self._process.stdout

    def read(self, chunkize: Optional[int] = None) -> bytes:
        if self._process is not None and self._process.poll() is not None:
            if not self.repeat_input and self._process.returncode == 0: # Clean exit
                time.sleep(0.1) # Don't let the process run full throttle
                return bytes()
            logger.warning(f"Subprocess reading file closed with code {self._process.returncode}. Restarting.")
            self._process = None
        return cast(bytes, self.get_stream().read(self.chunksize))

    def chunk_modify(self, chunk: bytes) -> bytes:
        return chunk

    def cleanup(self) -> None:
        if self._process is not None:
            logger.info("Closing ffmpeg process...")
            self._process.terminate()
            try:
                self._process.wait(timeout=5)
            except subprocess.TimeoutExpired:
                logger.warning("ffmpeg did not close. Killing...")
                self._process.kill()
            logger.info("Cleanup done.")
            self._process = None

    def set_input(self, input: str) -> None:
        self.url = input

class PortaudioStream(BaseAdapter):
    def __init__(self, **kwargs) -> None:
        self.input_id: Optional[int]             = None
        self._stream:  Optional[pyaudio.Stream]  = None
        self._pyaudio: Optional[pyaudio.PyAudio] = None
        super().__init__(format=pyaudio.paInt16, chunksize=kwargs["chunksize"])

    def get_stream(self, **kwargs) -> pyaudio.Stream:
        if self.input_id is None:
            raise BugException()
        if self._stream is None:
            p = self.pyaudio
            self._stream = p.open(
                format              = self.format,
                input_device_index  = self.input_id,
                channels            = self.channel_count,
                rate                = self.rate,
                input               = True,
                frames_per_buffer   = self.chunksize)
        return self._stream

    def read(self, chunksize: Optional[int] = None) -> bytes:
        return cast(bytes, self.get_stream().read(self.chunksize, exception_on_overflow=False))

    def chunk_modify(self, chunk: bytes) -> bytes:
        if self.chosen_channel is not None and self.channel_count > 1:
            # filter out specific channel using numpy
            logging.info("Using numpy to filter out specific channel.")
            data = np.fromstring(chunk, dtype='int16').reshape((-1, self.channel_count))
            data = data[:, self.chosen_channel - 1]
            if watchdog:
                watchdog.sent_audio(data)
            chunk = data.tostring()
        return chunk

    def cleanup(self) -> None:
        if self._stream is not None:
            self._stream.stop_stream()
            logger.debug("Stopped pyaudio stream")
            self._stream.close()
            logger.debug("Closed pyaudio stream")

        if self._pyaudio is not None:
            self._pyaudio.terminate()
            logger.debug("Terminated pyaudio")

    def set_input(self, id: int) -> None:
        devices = self.get_audio_devices()
        try:
            devName = devices[id]
            logger.info(f'Using audio input device: {id} ({devName})')
            self.input_id = id
        except (ValueError, KeyError) as e:
            logger.error(f'Unknown audio device: {id}')
            self.print_all_devices()
            sys.exit(1)

    def get_audio_devices(self) -> Dict[int, str]:
        devices = {}

        p = self.pyaudio
        info = p.get_host_api_info_by_index(0)
        deviceCount = info.get('deviceCount')

        for i in range(0, deviceCount):
                if p.get_device_info_by_host_api_device_index(0, i).get('maxInputChannels') > 0:
                        devices[i] = p.get_device_info_by_host_api_device_index(0, i).get('name')
        return devices

    def print_all_devices(self) -> None:
        """
        Special command, prints all audio devices available
        """
        print('-- AUDIO devices:')
        devices = self.get_audio_devices()
        for key in devices:
            dev = devices[key]
            if isinstance(dev, bytes):
                dev = dev.decode("ascii", "replace")
            print('    id=%i - %s' % (key, dev))

    @property
    def pyaudio(self) -> pyaudio.PyAudio:
        if self._pyaudio == None:
            self._pyaudio = pyaudio.PyAudio()
        return self._pyaudio

    def set_audio_channel_filter(self, channel: int) -> None:
        # actually chosing a specific channel is apparently impossible with portaudio,
        # so we record all channels instead and then filter out the wanted channel with numpy
        if self.input_id is None:
            raise BugException()
        channelCount = self.pyaudio.get_device_info_by_host_api_device_index(0, self.input_id).get('maxInputChannels')
        logger.info('Recording channel', channel, 'of', channelCount)
        self.channel_count = channelCount
        self.chosen_channel = channel



class RawAudioStream(BaseAdapter):
    def __init__(self):
        raise NotImplementedError()
