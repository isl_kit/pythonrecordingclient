#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from builtins import str, object
from typing import Callable, Optional, Tuple
import logging
import socket
import struct
import traceback
import signal
import re
from time import sleep
from threading import Thread, current_thread
from queue import Queue

logger = logging.getLogger("prc")

try:
    import ssl
except ImportError:
    logger.warning("ssl is not available in this python installation. Undefined behaviour can occur and related functionality will fail. It is recommended that you install python with ssl support.")

    # Create a pseudo ssl import to not create errors on try/except
    class _PseudoSSL():
        class _PseudoError(Exception):
            pass
        SSLWantReadError = _PseudoError
        SSLWantWriteError = _PseudoError
        def wrap_socket(self, socket):
            raise NotImplementedError("ssl is not available")
    ssl = _PseudoSSL() # type: ignore

class XMLSocket(object):

    MAX_LENGTH = 64*1024*1024

    # Regulates the sensibility of the congestion detector. Bigger value, less sensitive to short spikes
    CONGESTION_DETECT_LIMIT = 15

    def __init__(self,
            host_address: str, port: int,
            message_handler: Callable[[str], None] = None,
            close_handler:   Callable[[int], None] = None,
            interactive: bool = False,
            http_tunnel: Optional[str] = None
    ) -> None:
        """
        Arguments:
            http_tunnel:
                None or "(https?://)?(address/hostname):(port)?" if the connection should be proxied via http
        """
        self.socket: Optional[socket.socket] = None
        self.running = True

        self.host_address = host_address
        self.port = port
        self.q: Queue[Optional[str]] = Queue()
        self._q_size_old = 0
        self.t: Optional[Thread] = None
        self.t2: Optional[Thread] = None
        self.close_handler = close_handler
        self.message_handler = message_handler
        self._disconnected = True
        self._network_congestion_warned = False
        self.interactive = interactive
        self.http_tunnel = http_tunnel
        self._connect_buffer: Optional[bytes] = None

    @property
    def uses_tunnel(self) -> bool:
        return self.http_tunnel is not None

    @property
    def uses_tls_tunnel(self) -> bool:
        return self.uses_tunnel \
                and self.http_tunnel is not None \
                and re.match(r'https://', self.http_tunnel) is not None

    def __str__(self) -> str:
        return "XMLSocket {}:{}".format(self.host_address, self.port)

    def connect(self) -> None:
        self._disconnected = False
        self._network_congestion_warned = False
        self._q_size_old = 0

        self.t = Thread(name=str(self) + "Connect loop", target=self._connect)
        self.t.daemon = True
        self.t.start()
        self.t2 = Thread(name=str(self) + "Queue loop", target=self._read_queue)
        self.t2.daemon = True
        self.t2.start()
        self.t_netw = Thread(name=str(self) + "Congestion detector", target=self.detectSlowConnection)
        self.t_netw.daemon = True
        self.t_netw.start()

    def _shutdown_socket(self) -> None:
        if self.socket is None:
            return
        try:
            self.socket.shutdown(socket.SHUT_RDWR)
            self.socket.close()
        except:
            pass

    def close(self, exit_code: int = 0) -> None:
        """
        Closes this object.
        Subsequent calls will immediately return.
        """

        if not self.running:
            logger.warning("Unneeded close call to xml socket.")
            logger.debug(traceback.format_stack())
            return

        self.running = False
        self.q.put(None)

        if self.t_netw.is_alive():
            self.t_netw.join(1)

        # Wait for send queue to stop so we can close the socket cleanly
        # On interactive ask the user after a few seconds if he wants to keep waiting
        # Non-interactive without timeout is dangerous so we just wait 10 seconds
        if self.t2 is not None and self.t2 != current_thread():
            logger.info("Waiting for remaining data to be sent..")
            if self.interactive:
                self.t2.join(3)
                if self.t2.is_alive():
                    # Note: signals are not available in this thread so we create a thread for user input and wait until either finished
                    logger.info("To exit anyway, press the enter key. This will possibly lead to some audio not being sent.")
                    uinput_tmp = Thread(target=input)
                    uinput_tmp.start()
                    while uinput_tmp.is_alive() and self.t2.is_alive():
                        sleep(0.1)
            else:
                self.t2.join(10)
        if not self.q.empty():
            logger.warning("The audio data queue was not empty. Not all audio was sent.")
        else:
            logger.info("Done.")

        # Close the socket
        self._shutdown_socket()

        # After the socket is closed, the receive loop unblocks and we wait until it exits on next loop
        if self.t is not None and self.t != current_thread():
            self.t.join(3)
            if self.t.is_alive():
                logger.warning("Socket data receiver did not exit timely. This could be the indication of a problem.")

        if self.close_handler:
            self.close_handler(exit_code)

    def detectSlowConnection(self) -> None:
        # Logic for detecting and warning of network congestion.
        while self.running:
            if not self._disconnected:
                qsize = self.q.qsize()
                if qsize - self._q_size_old > XMLSocket.CONGESTION_DETECT_LIMIT:
                    if not self._network_congestion_warned:
                        logger.warning("Network connection might be slow or congested.")
                        self._network_congestion_warned = True
                    self._q_size_old = qsize
                elif self._q_size_old - qsize > XMLSocket.CONGESTION_DETECT_LIMIT:
                    if self._network_congestion_warned:
                        logger.warning("Network connection returned to being stable.")
                        self._network_congestion_warned = False
                    self._q_size_old = qsize
            sleep(0.1)

    def sendXML(self, xmlString: str) -> None:
        self.q.put(xmlString)
        logger.debug("Queue length is now {}".format(self.q.qsize()))

    def sendString(self, msg_str: str) -> None:
        """
        Sends the string in msg by prepending it with the message length
        """
        assert self.socket is not None

        # Mediator is very touchy about message encoding. So we try to encode everything as ascii
        # and in case it fails we warn the user and just send it with offenders replaced.
        # Usually this should not happen since the data will be base64 and the title urlencoded.
        try:
            msg = msg_str.encode("ascii", "strict")
        except UnicodeError as e:
            logger.warning("Message contains one or more non-ascii characters.")
            logger.warning( "First offender: {}. Replacing, but output might be garbled."
                    .format(e.object[e.start:e.end])) # type: ignore
            logger.debug(e.reason) # type: ignore
            msg = msg_str.encode("ascii", "replace")

        prefix = struct.pack("<L", socket.htonl(len(msg)))
        try:
            self.socket.sendall(prefix + msg)
            if self._disconnected:
                self._disconnected = False
                logger.info("The connection has been restored.")
        except socket.timeout:
            if not self._disconnected:
                logger.warning("Timeout on sending data. The connection might be broken and data lost. Trying to reconnect.")
            self._disconnected = True
        except ssl.SSLWantReadError as e:
            logger.error(f"Tried to write but ssl socket wants to read: '{e}'")
            self.close()
        except Exception as e:
            if self.running:
                logger.error("Error sending message {} with length {}".format(str(msg), len(msg)))
                logger.error(e)
                self.close()
            self._disconnected = True

    def _read_queue(self) -> None:
        while self.running or not self.q.empty():
            v = self.q.get()
            if not v:
                break
            self.sendString(v)

    @property
    def socket_target_tuple(self) -> Tuple[str, int]:
        """
        Helper function to return the a tuple containing hostname and port
        to where the socket of this object should connect to.
        """
        if self.uses_tunnel:
            assert self.http_tunnel is not None # always true but make mypy happy
            target = self.http_tunnel
            port = 80
            if re.match(r'https?://', target):
                http, target = target.split(':', 1)
                target = target[2:]
                port = 443 if re.match(r'https', http) else 80
            if re.match(r'.*:\d+$', target):
                tmp = target.split(':')
                target  = ''.join(tmp[:-1])
                port    = int(tmp[-1])
            return (target, port)
        else:
            return (self.host_address, self.port)

    def _upgrade_to_tls_tunnel(self) -> None:
        """
        Upgrades the socket connection to the proxy server to tls.
        Mediator doesn not support tls as of writing this (09.2020).
        This is used in combination with a http proxy.
        """
        assert self.socket is not None
        self.socket = ssl.wrap_socket(self.socket)

    def _complete_tunnel_connection(self) -> bytes:
        """
        Complete the http tunnel creation by sending the HTTP CONNECT and
        waiting/parsing until the end of proxy server response (empty line).

        Returns some data that has been read from the socket but is not related to the tunnel creation.
        Originally this function used MSG_PEEK to circumvent this, but this does not work
        with the tls socket anymore.
        """
        assert self.socket is not None
        RD_SIZE = 4096
        BOUND = b'\r\n\r\n'

        try:
            if self.uses_tls_tunnel:
                self._upgrade_to_tls_tunnel()
        except NotImplementedError as e:
            logger.error(f"Could not upgrade proxy connection to tls: '{e}'")
            self.close()
            raise

        # Send tunnel request
        host = self.host_address
        port = self.port
        request = f"CONNECT {host}:{port} HTTP/1.1\r\nHost: {host}:{port}\r\n\r\n"
        logger.debug(f"Sending tunnel request: {repr(request)}")
        self.socket.sendall(request.encode("ascii"))
        logger.debug("Tunnel request sent.")

        # Wait for answer
        found_pos = -1
        buf = bytes()
        while found_pos == -1:
            try:
                buf += self.socket.recv(RD_SIZE)
            except BlockingIOError:
                logger.warning("Socket not blocking TODO") # TODO what to do
                continue
            if not buf:
                logger.error("No valid HTTP-answer returned after proxy request.")
                raise Exception()
            found_pos = buf.find(BOUND, 0) # BOUND might be cut off so take the perf hit from always searching from 0

        # Take answer from buffer and parse
        tunnel_resp_data = buf[:found_pos+len(BOUND)] # Get Proxy response
        tunnel_resp = tunnel_resp_data.decode('ascii').split('\r\n', 1) # Split response into Header and Tail
        http_version, code, msg = tunnel_resp[0].split(' ', 2) # Parse Header
        logger.debug(f"Tunnel response: '{tunnel_resp}'")
        if int(code) // 100 != 2:
            raise Exception(f"Tunnel request failed. Response: '{repr(tunnel_resp)}'")
        assert len(tunnel_resp_data) == found_pos+len(BOUND)
        return buf[found_pos+len(BOUND):]

    def _connect(self) -> None:
        attempts = 0
        info_msg = f"Connecting to Socket Server {self.host_address}:{self.port}"
        if self.uses_tunnel:
            info_msg += f" via {'tls ' if self.uses_tls_tunnel else ''}proxy {self.socket_target_tuple}"
        logger.info(info_msg)
        additional_data = bytes()
        while self.running:
            try:
                self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
                self.socket.settimeout(10)
                connection_target = self.socket_target_tuple
                self.socket.connect(connection_target)
                if self.uses_tunnel:
                    try:
                        additional_data = self._complete_tunnel_connection()
                    except Exception as e:
                        logger.error(f"Tunnel creation failed: {e}")
                        raise
                logger.info("Connection Successful!")
                break
            except Exception as e:
                logger.debug(f"{type(e)} | {e}")
                try:
                    assert self.socket is not None
                    self.socket.close()
                except:
                    pass
                attempts += 1
                if attempts > 10:
                    logger.error("could not connect to mediator! Ending...")
                    self.close(exit_code=4)
                    return
                delay = min(attempts * attempts / 50.0, 10)
                logger.info("Connection failed...reconnect in " + str(delay) + " sec.")
                sleep(delay)
        self._receive(additional_data)

    def _receive(self, msg: bytes = bytes()) -> None:
        """
        Will block and possibly not return until self.socket.shutdown() is called. close() Should call that before joining.

        This documentation might be wrong or incomplete.
            (State 0): Read four bytes which contain the length of the next message. -> State 1
            (State 1): Then read in loop until that amount of bytes from the socket and send it to the handler -> State 0

        `msg` is an optional bytes-object that can contain data that was read beforehand, for example
        during tunnel creation
        """
        assert self.socket is not None

        msg_len    = 4
        state = 0
        while self.running:
            try:
                chunk = self.socket.recv(msg_len)
            except socket.timeout:
                continue # Note: To detect disconnects on send we set the socket to timeout. We want to wait forever on recv so we just ignore it here.
            except BlockingIOError as e: # In case of ssl, not enough data might be available
                logger.debug(f"Blocking error (not critical) on recv: {e}")
                continue
            except ssl.SSLWantWriteError as e:
                logger.error(f"Tried to read but ssl socket wants to write: '{e}'")
                self.close()
                return
            except:
                if self.running:
                    logger.error(traceback.format_exc())
                    self.close()
                return

            if len(chunk) == 0:
                if self.running:
                    logger.error("CONNECTION LOST")
                    self.close(5)
                return
            if state == 0:
                # First four bytes are an unsigned long that need to be converted
                msg_len = socket.htonl(struct.unpack("<L", chunk)[0])
                state = 1
            else:
                msg_len -= len(chunk)
                msg += chunk
                assert msg_len >= 0
                if msg_len == 0:
                    msg_len = 4
                    state = 0
                    if self.message_handler:
                        self.message_handler(msg.decode(encoding="ascii", errors="replace"))
                    msg = b''
