#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from typing import List, Callable, Optional
from builtins import object
from urllib.parse import quote
import atexit

from .mediatorInterface import MediatorInterface
from .mediatorLib import Auth
from .timedBuffer import TimedBuffer
from .config import Config

class Session(object):

    handler: List[Callable]  = []
    instances: List["Session"] = []
    receiver: List[Callable] = []

    def __init__(self,
            name: str = "TestSession",
            description: str ="",
            password: str ="secret",
            input_fingerprint: str = "de-DE",
            input_types: List[str] = ["audio"],
            output_fingerprint: List[str] = [],
            logging: bool = False,
            interactive: bool = False,
            zoom_url: str = None,
            zoom_quick_cc: bool = False,
            auth: Optional[Auth] = None
    ) -> None:
        """
        TODO finish documentation

        Members:
            id:
                The Session id as gotten from the Mediator
            name:
                The whole name of the session as specified
            title:
                The name without additional title parameters ('?'-delimited) as it appears on an LT instance
            description:
                The description as specified
            interactive:
                Set if the user is able to interact with the session. For example in a command line
            auth:
                Tuple (user, pass, server) to enable authentication or None to disable. pass can be None to ask at connection time..
            output_fingerprint:
                List of strings of outputs that should be requested to be returned. The format for each string
                is fingerprint[:type], for example "en:unseg-text". If type not supplied, "text" is implied.
                This format is only used in the PRC internally. The type must be text-based, eg audio is not supported.
                In case you want to use a theoretical possible fingerprint containing a ':', you cannot leave the
                type implicit and must provide it manually.
        """
        if zoom_url:
            name += "?ZOOM_URL=" + quote(zoom_url) + ("?ZOOM_LANG=" + output_fingerprint[0]) if len(output_fingerprint) > 0 else ""
            name += "?ZOOM_QUICK_SUBTITLES" if zoom_quick_cc else ""
        self.id: Optional[int] = None
        self.name = quote(name)
        self.description = description
        self.password = password
        self.input_types = input_types
        self.input_fingerprint = input_fingerprint
        self.output_fingerprint = output_fingerprint
        self.text_msg_delta = 10
        self.logging = logging
        self.sender = None
        self.ready = False
        self.closed = False
        self.buffer = TimedBuffer(Config.CHUNK_SIZE, accurate_time=Config.CORRECT_TIME)
        self.interactive = interactive
        self.auth = auth

        Session.instances.append(self)

    @property
    def title(self) -> str:
        return self.name.split("?", 1)[0]

    def _ready(self) -> None:
        self.ready = True
        for handler in Session.handler:
            handler(self)

    def _receive(self, message) -> None:
        for receiver in Session.receiver:
            receiver(self, message)

    def _close(self, exit_code: int) -> None:
        self.closed = True
        self.ready = False

    def start(self, host_address: str, port: int, http_tunnel: Optional[str] = None) -> None:
        self.interface = MediatorInterface(
                host_address, port, self,
                ready_handler=self._ready, receive_handler=self._receive, close_handler=self._close,
                http_tunnel=http_tunnel
        )
        self.interface.connect()

    def send_text(self, text: str) -> None:
        self.interface.send_text(text)

    def send_audio(self, chunk: bytes) -> None:
        self.interface.send_audio(chunk)

    def send_video(self, chunk) -> None:
        self.interface.send_video(chunk)

    def stop(self) -> None:
        self.closed = True
        self.ready = False
        if self.interface:
            self.interface.close()


def on_session_ready(func: Callable) -> Callable:
    Session.handler.append(func)
    return func


def on_receive(func: Callable) -> Callable:
    Session.receiver.append(func)
    return func


# def signal_handler(*args) -> None:
#     for instance in Session.instances:
#         if not instance.closed:
#             instance.stop()

# atexit.register(signal_handler)
