#!/usr/bin/python
# -*- coding: utf-8 -*-

from setuptools import setup

setup(
    name="PythonRecordingClient",
    version="2.4.1",
    author="Bastian Krüger",
    author_email="bastian.krueger@kit.edu",
    maintainer="Sebastian Stüker",
    maintainer_email="sebastian.stueker@karlsruhe-it-solutions.com",
    packages=["pythonrecordingclient"],
    scripts=[
        "bin/recording_client_file.py",
        "bin/recording_client_live.py",
        "bin/recording_client_text.py",
        "bin/recording_client_ffmpeg_stream.py",
        "bin/python_recording_client.py"
    ],
    url="https://bitbucket.org/isl_kit/pythonrecordingclient",
    license="LICENSE.txt",
    description="A python tool, that connects as client to a Mediator and allows starting sessions and streaming audio.",
    long_description=open("README.md").read(),
    install_requires=[
        'PyAudio>=0.2.11',
        'requests>=2.18.1',
        'numpy'
    ],
    zip_safe = True,
    python_requires='>3.6',
)
